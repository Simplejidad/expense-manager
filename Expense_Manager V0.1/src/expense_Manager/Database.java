package expense_Manager;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Scanner;
import java.sql.ResultSet;

public class Database {
	
	static Scanner input = new Scanner(System.in);
	private Connection con = null;
	private final String user = "sa";
	private final String pass = "rastrear";
	private final String db = "Expense_Manager";
	
	public Database() {
		
		try {
			
			String url = "jdbc:jtds:Sqlserver://localhost:1433;" + db;
						
			Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			
			con = DriverManager.getConnection(url, user, pass);
			
			if (con != null) {
				System.out.println("Connection with " + db + " successful!");
			}
		}
		
		catch (Exception ex){
			ex.printStackTrace();
		}
	}
	
public void addExpenseCategory(String category){
		
		
		try {
			if (con != null) {
						
				Statement statement = con.createStatement();
				statement.executeUpdate("INSERT INTO Expense_Categories (Expense_Categorie) VALUES('" + category + "')");
				
				con.close();
				
				System.out.println("Category added successfully");
			}
		} catch (Exception e) {
			
			System.err.println("Got an exception!");
			System.err.println(e.getMessage());
		}
	}

public void addAccount(String accountName, float openingBalance ){
	
	
	try {
		if (con != null) {
			
			Statement statement = con.createStatement();
			statement.executeUpdate("INSERT INTO Accounts (Account, Balance, Opening_Balance) VALUES('" + accountName + "', " + openingBalance + ", " + openingBalance + ")");
			
			con.close();
			
			System.out.println("Account " + accountName + " added successfully with $" + openingBalance + " opening balance.");
			
		}
	} catch (Exception e) {
		
		System.err.println("Got an exception!");
		System.err.println(e.getMessage());
	}
}

public void addExpense(float ammount, int originAccountID, int categoryID, String date, String notes){
	
	int transactionTypeID = 2;
	
	try {
		
		if (con != null) {
				
			Statement statement = con.createStatement();
			String SQLQuerry = "INSERT INTO Transactions (Type_ID, Ammount, Origin_Account_ID, Category_ID, Date, Notes)"
					+ " VALUES(" + transactionTypeID + " , " + ammount + " , " + originAccountID + " , " +  categoryID + " , '" + date +"' , '" + notes + "')";
			//System.out.println(SQLQuerry);
			statement.executeUpdate(SQLQuerry);
			
			this.substractBalance(ammount, originAccountID);
					
			con.close();
			
			System.out.println("Expense added successfully");
		}
	} catch (Exception e) {
		
		System.err.println("Got an exception!");
		System.err.println(e.getMessage());
	}
	
}

public void addIncome(float ammount, int destinyAccountID, int categoryID, String date, String notes){
	
	int transactionTypeID = 1;
		
	try {
		
		if (con != null) {
				
			Statement statement = con.createStatement();
			String SQLQuerry = "INSERT INTO Transactions (Type_ID, Ammount, Destiny_Account_ID, Category_ID, Date, Notes)"
					+ " VALUES(" + transactionTypeID + " , " + ammount + " , " +  destinyAccountID + " ," + categoryID + " , '" + date +"' , '" + notes + "')";
			System.out.println(SQLQuerry);
			statement.executeUpdate(SQLQuerry);
			
			this.addBalance(ammount, destinyAccountID);
								
			con.close();
			
			System.out.println("Income added successfully");
		}
	} catch (Exception e) {
		
		System.err.println("Got an exception!");
		System.err.println(e.getMessage());
	}
	
}

public void addTransfer(float ammount, int destinyAccountID, int originAccountID, String date, String notes){
	
	int transactionTypeID = 3;
	
	try {
		
		if (con != null) {
				
			Statement statement = con.createStatement();
			String InsertStatement = "INSERT INTO Transactions (Type_ID, Ammount, Origin_Account_ID, Destiny_Account_ID, Date, Notes)"
					+ " VALUES(" + transactionTypeID + " , " + ammount + " , " + originAccountID + " ," +  destinyAccountID + " ,'" + date +"' , '" + notes + "')";
			//System.out.println(SQLQuerry);
			statement.executeUpdate(InsertStatement);
			
			this.substractBalance(ammount, originAccountID);
			this.addBalance(ammount, destinyAccountID);
									
			con.close();
			
			System.out.println("Transfer added successfully");
		}
		
	} catch (Exception e) {
		
		System.err.println("Got an exception!");
		System.err.println(e.getMessage());
	}
	
}

private void addBalance(float ammount, int accountID){
	
try {
		
		if (con != null) {
				
			Statement statement = con.createStatement();
			String UpdateStatement = "UPDATE Accounts SET Balance = (Balance + " + ammount + ") WHERE ID = " + accountID + ";";
//			System.out.println(UpdateStatement);
			statement.executeUpdate(UpdateStatement);
						
			System.out.println("Balance added successfully");
		}
		
	} catch (Exception e) {
		
		System.err.println("Got an exception!");
		System.err.println(e.getMessage());
	}
}

private void substractBalance(float ammount, int accountID){
	
try {
		
		if (con != null) {
				
			Statement statement = con.createStatement();
			String UpdateStatement = "UPDATE Accounts SET Balance = (Balance - " +  ammount + ") WHERE ID = " + accountID + ";";
			//System.out.println(UpdateStatement);
			statement.executeUpdate(UpdateStatement);
			
			System.out.println("Balance substracted successfully");
		}
		
	} catch (Exception e) {
		
		System.err.println("Got an exception!");
		System.err.println(e.getMessage());
	}
}

public void showMainStats(){
	
	try {
		
	if (con != null) {
		
	System.out.println("---HERE ARE YOUR MAIN STATS---");

	Statement statement = con.createStatement();
	
	String netWorthQuery = "SELECT SUM(Balance) as Net_Worth FROM Accounts;";
	ResultSet rs = statement.executeQuery(netWorthQuery);
	rs.next();
	float netWorth = rs.getFloat("Net_Worth");
	System.out.println("Net Worth: $" + netWorth);
	
	String currentMonthExpensesQuery = "SELECT SUM(Ammount) AS MonthsExpenses FROM Transactions WHERE MONTH(Date) = " 
	+ (Calendar.getInstance().get(Calendar.MONTH) + 1) + " AND Type_ID = 2;";
	rs = statement.executeQuery(currentMonthExpensesQuery);
	rs.next();
	float thisMonthsExpenses = rs.getFloat("MonthsExpenses");
	System.out.println("This month's expenses: $" + thisMonthsExpenses);
	
	String currentMonthIncomeQuery = "SELECT SUM(Ammount) AS MonthsIncome FROM Transactions WHERE MONTH(Date) = "
	+ (Calendar.getInstance().get(Calendar.MONTH) + 1) + " AND Type_ID = 1;";
	rs = statement.executeQuery(currentMonthIncomeQuery);
	rs.next();
	float thisMonthsIncome = rs.getFloat("MonthsIncome");
	System.out.println("This month's income: $" + thisMonthsIncome);
	
	System.out.println("This moth's balance: $" + (thisMonthsIncome - thisMonthsExpenses));
	
	System.out.println("--------------------------------------");
		
		}
		
	} catch (Exception e) {
			
			System.err.println("Got an exception!");
			System.err.println(e.getMessage());
		}
	}



public void showAccounts(){
	
	try {
		
		if (con != null) {
			
			Statement statement = con.createStatement();
			
			String allAccountsQuery = "SELECT ID, Account, Balance FROM Accounts;";
			ResultSet rs = statement.executeQuery(allAccountsQuery);
			
			System.out.print("THESE ARE YOUR ACCOUNTS");
			
			while (rs.next()) {
				
				int accountID = rs.getInt("ID");
				String accountName = rs.getString("Account");
				float accountBalance = rs.getFloat("Balance");
				
				System.out.println(accountID + " - " + accountName + ": $" + accountBalance );
				
			}
			
			System.out.println("----------------------------");
			
		}
	
		} catch (Exception e) {
			
			System.err.println("Got an exception!");
			System.err.println(e.getMessage());
		}
	
	}

public void showExpenses(){
	
	try {
		
		if (con != null) {
			
			Statement statement = con.createStatement();
			
			String expensesQuery = "SELECT * FROM Transactions"
					+ " JOIN Expense_Categories "
					+ " ON Transactions.Category_ID = Expense_Categories.ID "
					+ " WHERE Type_ID = 2 AND MONTH(Date) = MONTH(GETDATE()) AND YEAR(Date) = YEAR(GETDATE())"
					+ " ORDER BY Date;";
			
			ResultSet rs = statement.executeQuery(expensesQuery);
			
			System.out.println("Your Expenses this month:");
			
			while (rs.next()) {
				
				String date = rs.getString("Date");
				float ammount = rs.getFloat("Ammount");
				String category = rs.getString("Category");
				String notes = rs.getString("Notes");
				
				
				System.out.println(date + " - " + category +  ": $" + ammount + " " + notes);
				
			}
			
			System.out.println("----------------------------");
			
		}
		
	
		} catch (Exception e) {
			
			System.err.println("Got an exception!");
			System.err.println(e.getMessage());
		}
	
	}

public void showIncome(){
	
	try {
		
		if (con != null) {
			
			Statement statement = con.createStatement();
			
			String incomeQuery = "SELECT * FROM Transactions"
					+ " JOIN Income_Categories "
					+ " ON Transactions.Category_ID = Income_Categories.ID "
					+ " WHERE Type_ID = 1 AND MONTH(Date) = MONTH(GETDATE()) AND YEAR(Date) = YEAR(GETDATE()) "
					+ " ORDER BY Date;";
			
			ResultSet rs = statement.executeQuery(incomeQuery);
			
			System.out.println("Your Income this month:");
			
			while (rs.next()) {
				
				String date = rs.getString("Date");
				float ammount = rs.getFloat("Ammount");
				String category = rs.getString("Category");
				String notes = rs.getString("Notes");
				
				
				System.out.println(date + " - " + category +  ": $" + ammount + " " + notes);
				
			}
			
			System.out.println("----------------------------");
			
		}
	
		} catch (Exception e) {
			
			System.err.println("Got an exception!");
			System.err.println(e.getMessage());
		}
	
	}
}
