package expense_Manager;

import java.util.Calendar;
import java.util.Scanner;


public class ExpenseManager {
	
	static Scanner input = new Scanner(System.in);
	static boolean idle = true;
	public static String separator = "----------------------------------------";
	
	public static void main(String[] args) {
		
		while (idle) {
		
		showMainStats();
				
		}
		
		//addExpenseCategory("Citas");
		//addExpense(111, 1, 2, "2015-09-10", "Gasto de Septiembre");
		//addTransfer(1000, 4, 1, "2015-09-09", "Primera Transferencia");
		//addIncome(150, 1, 4, "2015-09-09", "Other Income");
	
	}
	
			
	
	private static void addExpenseCategory(String category){
		
		Database data = new Database();
		
		data.addExpenseCategory(category);
		
		switchIdle();
	}
	
	private static void addAccount(){
		
		System.out.println("You are creating a New Account!");
		
		String accountNameRequest = "Write the name of the new Account, then press Enter. (leave blank to CANCEL)";
		String accountName = inputString(accountNameRequest);
		
		if (accountName.equals("")){
			
			System.out.println("Cancelled creating new account. Back to the Main Menu.");
			
			showMainMenu();
			
		} else {
			
			String openingBalanceRequest = "Please enter the Opening Balance for the new Account (ammount only, no currency symbols), then press Enter";
			float openingBalance = inputFloat(openingBalanceRequest);
			
			addAccount(accountName, openingBalance);
			
		}		
	}
	
	private static void addAccount(String account, float openingBalance){
		
		Database data = new Database();
		
		data.addAccount(account, openingBalance);
		
		switchIdle();
	}
	
	
	private static void addExpense(){
		
		String ammountRequest = "How much would did you spend? Write the number, then press Enter";
		float ammount = inputFloat(ammountRequest);
		
		String originAccountIDRequest = "Where did the money fro your expense come from?\nEnter the ID number of the account, then press Enter.";
		int originAccountID = inputInt(originAccountIDRequest);
		
		String categoryIDRequest = "What kind of expense whas this? Enter de id of the category, then press Enter";
		int categoryID = inputInt(categoryIDRequest);
		
		String dateRequest = "When did you make this expense? Enter the date of the transaction in the format YYYY-MM-DD";
		String date = inputDate(dateRequest);
		
		String notesRequest = "Enter any notes you may want to add to this transaction.";
		String notes = inputString(notesRequest);
		
		addExpense(ammount, originAccountID, categoryID, date, notes);
		
	}
	
	private static void addExpense(int originAccountID){
		
		String ammountRequest = "How much would did you spend? Write the number, then press Enter";
		float ammount = inputFloat(ammountRequest);
		
		String categoryIDRequest = "What kind of expense whas this? Enter de id of the category, then press Enter";
		int categoryID = inputInt(categoryIDRequest);
		
		String dateRequest = "When did you make this expense? Enter the date of the transaction in the format YYYY-MM-DD";
		String date = inputDate(dateRequest);
		
		String notesRequest = "Enter any notes you may want to add to this transaction.";
		String notes = inputString(notesRequest);
		
		addExpense(ammount, originAccountID, categoryID, date, notes);
		
	}
	
	private static void addExpense(float ammount, int originAccountID, int categoryID, String date, String notes) {
		
		Database data = new Database();
		
		data.addExpense(ammount, originAccountID, categoryID, date, notes);
		
		switchIdle();
	}

	
	private static void addIncome(){
		
		String ammountRequest = "How much did you make? Enter the number then press Enter.";
		float ammount = inputFloat(ammountRequest);
		
		String destinyAccountIDRequest = "Where did you put all those moneys? Enter the account ID.";
		int destinyAccountID = inputInt(destinyAccountIDRequest);
		
		String categoryIDRequest = "How did you do this? Enter the ID of the category";
		int categoryID = inputInt(categoryIDRequest);
		
		String dateRequest = "When did you make the big bucks? Enter the date of the transaction in the format YYYY-MM-DD";
		String date = inputDate(dateRequest);
		
		String notesRequest = "Enter any notes you may want to add to this transaction.";
		String notes = inputString(notesRequest);
		
		addIncome(ammount, destinyAccountID, categoryID, date, notes);
				
	}
	
	private static void addIncome(int destinyAccountID){
		
		String ammountRequest = "How much did you make? Enter the number then press Enter.";
		float ammount = inputFloat(ammountRequest);
				
		String categoryIDRequest = "How did you do this? Enter the ID of the category";
		int categoryID = inputInt(categoryIDRequest);
		
		String dateRequest = "When did you make the big bucks? Enter the date of the transaction in the format YYYY-MM-DD";
		String date = inputDate(dateRequest);
		
		String notesRequest = "Enter any notes you may want to add to this transaction.";
		String notes = inputString(notesRequest);
		
		addIncome(ammount, destinyAccountID, categoryID, date, notes);
				
	}
	
	private static void addIncome(float ammount, int destinyAccountID, int categoryID, String date, String notes) {
		
		Database data = new Database();
		
		data.addIncome(ammount, destinyAccountID, categoryID, date, notes);
				
	}

	private static void addTransfer(){
		
		String ammountRequest = "How much would you like to transfer? Write the number, then press Enter";
		float ammount = inputFloat(ammountRequest);
		
		String originAccountIDRequest = "Enter the ID of the account you are transfering From, then press Enter.";
		int originAccountID = inputInt(originAccountIDRequest);
		
		String destinyAccountIDRequest = "Write the ID of the account you are tranfering To, then press Enter.";
		int destinyAccountID = inputInt(destinyAccountIDRequest);
		
		String dateRequest = "Enter the date of the transaction in the format YYYY-MM-DD";
		String date = inputDate(dateRequest);
		
		String notesRequest = "Enter any notes you may want to add to this transaction.";
		String notes = inputString(notesRequest);
		
		addTransfer(ammount, originAccountID, destinyAccountID, date, notes);
		
	}
	
	private static void addTransferFrom (int originAccountID){
		
		String ammountRequest = "How much would you like to transfer? Write the number, then press Enter";
		float ammount = inputFloat(ammountRequest);
		
		String destinyAccountIDRequest = "Write the ID of the account you are tranfering To, then press Enter.";
		int destinyAccountID = inputInt(destinyAccountIDRequest);
		
		String dateRequest = "Enter the date of the transaction in the format YYYY-MM-DD";
		String date = inputDate(dateRequest);
		
		String notesRequest = "Enter any notes you may want to add to this transaction.";
		String notes = inputString(notesRequest);
		
		addTransfer(ammount, originAccountID, destinyAccountID, date, notes);
		
	}
	
	private static void addTransferTo(int destinyAccountID){
		
		String ammountRequest = "How much would you like to transfer? Write the number, then press Enter";
		float ammount = inputFloat(ammountRequest);
		
		String originAccountIDRequest = "Enter the ID of the account you are transfering From, then press Enter.";
		int originAccountID = inputInt(originAccountIDRequest);
		
		String dateRequest = "Enter the date of the transaction in the format YYYY-MM-DD";
		String date = inputDate(dateRequest);
		
		String notesRequest = "Enter any notes you may want to add to this transaction.";
		String notes = inputString(notesRequest);
		
		addTransfer(ammount, originAccountID, destinyAccountID, date, notes);
		
	}
	
	private static void addTransfer(float ammount,  int originAccountID, int destinyAccountID, String date, String notes){
	
		Database data = new Database();
		
		data.addTransfer(ammount, destinyAccountID, originAccountID, date, notes);
		
		switchIdle();
	}
	
	
	
	private static void doMainMenuAction(int actionNumber) {
		
		String actionRequest = "Select an action from the menu above by writing down it's number and pressing Enter.";
		
		switch (actionNumber) {
			case 1 : addExpense();
				break;
			case 2 : addIncome();
				break;
			case 3 : addTransfer();
				break;
			case 4 : showExpenses();
				break;
			case 5 : showIncome();
				break;
			case 6 : showAccounts();
			default: System.out.println("Invalid input, try again.");
					doMainMenuAction(inputInt(actionRequest));
				break;
		}
	}
	
	
	
	
	private static String inputDate(String request){
		
		String yearRequest = "Enter the year of the transaction";
		String monthRequest ="Enter the number of the month of the transaction";
		String dayRequest = "Enter the number of the day of the transaction";
		
		
		int year = inputInt(yearRequest);
		int month = inputInt(monthRequest);
		int day = inputInt(dayRequest);
		
		String date = year + "-" + month + "-" + day;
		return date;
		
	}
	
	private static float inputFloat(String request){
		
		while(true) {
			System.out.println(request);
			
			try {
				float ammount = input.nextFloat();
				input.nextLine();
				return ammount;
			} catch (java.util.InputMismatchException e) {
				
				input.nextLine();
				//System.out.println(request);
			}
		}
		
		
	}
	
	private static int inputInt(String request){
		
		while(true) {
			System.out.println(request);
			
			try {
				int number = input.nextInt();
				input.nextLine();
				return number;
			} catch (java.util.InputMismatchException e) {
				
				input.nextLine();
				//System.out.println(request);
			}
		}		
	}
	
	private static String inputString(String request) {
		
		while(true) {
			System.out.println(request);
			
			try {
				String someString = input.nextLine();
				return someString;
			} catch (java.util.InputMismatchException e) {
				
				input.nextLine();
				System.out.println(request);
			}
		}
	}
	
	private static void returnToMainMenu(){
		
		System.out.println("Enter anything to return to the main menu.");
		
		String anything = input.nextLine();
		
		showMainMenu();
	}
	
	private static void selectAccount(){
		
		String accountRequest = "Enter the Account ID number to select the Account, 0 to create a NEW Account,/n or anything else to go back to the main menu.";
		
		String instruction = inputString(accountRequest);
		
		try {
			
			int selectedAccount = Integer.parseInt(instruction);
			
			showAccountsMenu(selectedAccount);
			
		} catch (Exception e ) {
			
			System.out.println("You entered an invalid account number. Going back to the Main Menu:");
			
			showMainMenu();
		}
		
	}
		
	
	private static void showMainStats(){
		
		switchIdle();
		
		Database data = new Database();
		
		data.showMainStats();
		
		showMainMenu();
				
	}
	
	private static void showAccounts(){
		
		Database data = new Database();
		
		data.showAccounts();
		
		selectAccount();
		
	}
	
	private static void showExpenses(){
		
		Database data = new Database();
		
		data.showExpenses();
		
		returnToMainMenu();
		
	}
	
	private static void showIncome(){
		
		Database data = new Database();
		
		data.showIncome();
		
		returnToMainMenu();
		
	}
	
	private static void showMainMenu(){
		
		String actionRequest = "Select an action from the menu above by writing down it's number and pressing Enter.";
		
		System.out.println("1- Add Expense");
		System.out.println("2- Add Income");
		System.out.println("3- Add Transfer");
		System.out.println("4- View Expenses");
		System.out.println("5- View Income");
		System.out.println("6- View Accounts");
		System.out.println(separator);
		
		doMainMenuAction(inputInt(actionRequest));

				
	}
	
	
	
	private static void showAccountsMenu(int accountID){
		
		if (accountID == 0) {
			
			addAccount();
		}
				
		else {
		
		String actionRequest = "Select an action from the menu above by writing down it's number and pressing Enter.";
		
		System.out.println("1- Show records");
		System.out.println("2- Add Expense");
		System.out.println("3- Add Income");
		System.out.println("4- Edit/Delete ACcount");
		System.out.println("5- Transfer to");
		System.out.println("6- Transfer from");
		System.out.println("7- Move position");
		System.out.println("8- Cancel");
		System.out.println(separator);
		
		doAccountsAction(accountID, inputInt(actionRequest));
		
		}
		

				
	}
	
	private static void doAccountsAction(int accountID, int actionID) {
				
		switch (actionID) {
		case 1 : 
			break;
		case 2 : addExpense(accountID);
			break;
		case 3 : addIncome(accountID);
			break;
		case 4 : 
			break;
		case 5 : addTransferTo(accountID);
			break;
		case 6 : addTransferFrom(accountID);
			break;
		default: 
			break;
	}
	}



	private static void switchIdle(){
		if(idle){
			idle = false;
		} else {idle = true;}
	}
	
}
	





