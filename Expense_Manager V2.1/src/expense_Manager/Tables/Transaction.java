package expense_Manager.Tables;

public class Transaction {
	
	int id;
	int typeID;
	float ammount;
	int originAccountID;
	int destinyAccountID;
	int categoryID;
	String date;
	String notes;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTypeID() {
		return typeID;
	}
	public void setTypeID(int typeID) {
		this.typeID = typeID;
	}
	public float getAmmount() {
		return ammount;
	}
	public void setAmmount(float ammount) {
		this.ammount = ammount;
	}
	public int getOriginAccountID() {
		return originAccountID;
	}
	public void setOriginAccountID(int originAccountID) {
		this.originAccountID = originAccountID;
	}
	public int getDestinyAccountID() {
		return destinyAccountID;
	}
	public void setDestinyAccountID(int destinyAccountID) {
		this.destinyAccountID = destinyAccountID;
	}
	public int getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	

}
