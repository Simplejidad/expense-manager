package expense_Manager;

import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import expense_Manager.Tables.ExpenseCategory;


public class ExpenseManager {
	
	static Scanner input = new Scanner(System.in);
	static boolean idle = true;
	static String anything;
	public static String separator = "----------------------------------------";
	
	public static void main(String[] args) {
		
		//Auxiliary.showMainStats();
		
		Configuration config = new Configuration();
    	SessionFactory factory = (SessionFactory) config.configure().buildSessionFactory();
    	Session session = factory.openSession();
    	
    	createEspenseCategory(session);
		
	}		
	
    public static void createEspenseCategory(Session session) {
    	
        ExpenseCategory expCat = new ExpenseCategory();
        Transaction tx = null;
        
        expCat.setCategory("Probando");
    
     
        try{
            tx = session.beginTransaction();
            session.save(expCat); 
            tx.commit();
         }catch (Exception e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace(); 
         }
        
      }

	
	
	
	public static void doMainMenuAction(int actionNumber) {
		
		String actionRequest = "Select an action from the menu above by writing down it's number and pressing Enter.";
		
		switch (actionNumber) {
			case 1 : Expense.addExpense();
				break;
			case 2 : Income.addIncome();
				break;
			case 3 : Transfer.addTransfer();
				break;
			case 4 : Auxiliary.showExpenses();
				break;
			case 5 : Auxiliary.showIncome();
				break;
			case 6 : Auxiliary.showAccounts();
				break;
			default: System.out.println("Invalid input, try again.");
					doMainMenuAction(Auxiliary.enterInt(actionRequest));
				break;
		}
	}
	
	
	
	public static void returnToMainMenu(){
		
		System.out.println("Enter anything to return to the main menu.");
		
		anything = input.nextLine();
				
		showMainMenu();
	}
	
	public static void selectAccount(){
		
		String accountRequest = "Enter the Account ID number to select the Account, 0 to create a NEW Account,/n or anything else to go back to the main menu.";
		
		String instruction = Auxiliary.enterString(accountRequest);
		
		try {
			
			int selectedAccount = Integer.parseInt(instruction);
			
			showAccountsMenu(selectedAccount);
			
		} catch (Exception e ) {
			
			System.out.println("You entered an invalid account number. Going back to the Main Menu:");
			
			showMainMenu();
		}
		
	}	
	

	
	public static void showMainMenu(){
		
		String actionRequest = "Select an action from the menu above by writing down it's number and pressing Enter.";
		
		System.out.println("1- Add Expense");
		System.out.println("2- Add Income");
		System.out.println("3- Add Transfer");
		System.out.println("4- View Expenses");
		System.out.println("5- View Income");
		System.out.println("6- View Accounts");
		System.out.println(separator);
		
		doMainMenuAction(Auxiliary.enterInt(actionRequest));

				
	}
	
	
	
	public static void showAccountsMenu(int accountID){
		
		if (accountID == 0) {
			
			BAccount.addAccount();
		}
				
		else {
		
		String actionRequest = "Select an action from the menu above by writing down it's number and pressing Enter.";
		
		System.out.println("1- Show records");
		System.out.println("2- Add Expense");
		System.out.println("3- Add Income");
		System.out.println("4- Edit/Delete Account");
		System.out.println("5- Transfer to");
		System.out.println("6- Transfer from");
		System.out.println("7- Cancel");
		System.out.println(separator);
		
		doAccountsAction(accountID, Auxiliary.enterInt(actionRequest));
		
		}
		
	
	// TODO MIST after doing an action on an account, it goes back to the account action's menu
	
				
	}
	
	public static void doAccountsAction(int accountID, int actionID) {
				
		switch (actionID) {
		case 1 : Auxiliary.showAccountExpenses(accountID);
			break;
		case 2 : Expense.addExpense(accountID);
			break;
		case 3 : Income.addIncome(accountID);
			break;
		case 4 : 
			break;
		case 5 : Transfer.addTransferTo(accountID);
			break;
		case 6 : Transfer.addTransferFrom(accountID);
			break;
		case 7 : showMainMenu();
			break;
		default: 
			break;
		}
		
	}

	public static boolean isIdle() {
		return idle;
	}

	public static void setIdle(boolean idle) {
		ExpenseManager.idle = idle;
	}
	
}