package expense_Manager;

public class BAccount {
	
	static String accountNameRequest = "Write the name of the new Account, then press Enter. (leave blank to CANCEL)";
	static String openingBalanceRequest = "Please enter the Opening Balance for the new Account (ammount only, no currency symbols), then press Enter";
	static String newAccountWarning = "Your are creating a New Account!";
	static String cancelMessage = "Cancelled creating new account. Back to the Main Menu.";
	
	public BAccount(){
		
	}
	
	public static void addAccount(){
		
		System.out.println(newAccountWarning);
		
		String accountName = Auxiliary.enterString(accountNameRequest);
		
		if (accountName.equals("")){
			
			System.out.println(cancelMessage);
			
			Auxiliary.switchIdle();
			
		} else {
			
			float openingBalance = Auxiliary.enterFloat(openingBalanceRequest);
			
			addAccount(accountName, openingBalance);
			
		}		
	}
	
	public static void addAccount(String account, float openingBalance){
		
		Database data = new Database();
		
		data.addAccount(account, openingBalance);
		
	}

}
