package expense_Manager;

public class Transfer {
	
	static String ammountRequest = "How much would you like to transfer? Write the number, then press Enter";
	static String originAccountIDRequest = "Enter the ID of the account you are transfering From, then press Enter.";
	static String destinyAccountIDRequest = "Write the ID of the account you are tranfering To, then press Enter.";
	static String dateRequest = "Enter the date of the transaction in the format YYYY-MM-DD";
	static String notesRequest = "Enter any notes you may want to add to this transaction.";

	public Transfer(){
		
	}
	
	public static void addTransfer(){
		
		Database data = new Database();
		
		float ammount = Auxiliary.enterFloat(ammountRequest);
		int originAccountID = Auxiliary.enterInt(originAccountIDRequest);
		int destinyAccountID = Auxiliary.enterInt(destinyAccountIDRequest);
		String date = Auxiliary.enterDate(dateRequest);
		String notes = Auxiliary.enterString(notesRequest);
		
		data.addTransfer(ammount, originAccountID, destinyAccountID, date, notes);
		
	}
	
	public static void addTransferFrom (int originAccountID){
		
		Database data = new Database();
		
		float ammount = Auxiliary.enterFloat(ammountRequest);
		int destinyAccountID = Auxiliary.enterInt(destinyAccountIDRequest);
		String date = Auxiliary.enterDate(dateRequest);
		String notes = Auxiliary.enterString(notesRequest);
		
		data.addTransfer(ammount, originAccountID, destinyAccountID, date, notes);
		
	}
	
	public static void addTransferTo(int destinyAccountID){
		
		Database data = new Database();
		
		float ammount = Auxiliary.enterFloat(ammountRequest);
		int originAccountID = Auxiliary.enterInt(originAccountIDRequest);
		String date = Auxiliary.enterDate(dateRequest);
		String notes = Auxiliary.enterString(notesRequest);
		
		data.addTransfer(ammount, originAccountID, destinyAccountID, date, notes);
		
	}
	
	public static void addTransfer(float ammount,  int originAccountID, int destinyAccountID, String date, String notes){
	
		Database data = new Database();
		
		data.addTransfer(ammount, destinyAccountID, originAccountID, date, notes);
		
	}

}
