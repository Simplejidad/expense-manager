package expense_Manager;

public class Income {
	
	static String ammountRequest = "How much did you make? Enter the number then press Enter.";
	static String destinyAccountIDRequest = "Where did you put all those moneys? Enter the account ID.";
	static String categoryIDRequest = "How did you do this? Enter the ID of the category";
	static String dateRequest = "When did you make the big bucks? Enter the date of the transaction in the format YYYY-MM-DD";
	static String notesRequest = "Enter any notes you may want to add to this transaction.";
	
	
	public Income(){
		
	}
	
	public static void addIncome(){
		
		
		float ammount = Auxiliary.enterFloat(ammountRequest);
		int destinyAccountID = Auxiliary.enterInt(destinyAccountIDRequest);
		int categoryID = Auxiliary.enterInt(categoryIDRequest);
		String date = Auxiliary.enterDate(dateRequest);
		String notes = Auxiliary.enterString(notesRequest);
		
		addIncome(ammount, destinyAccountID, categoryID, date, notes);
				
	}
	
	public static void addIncome(int destinyAccountID){
		
		
		float ammount = Auxiliary.enterFloat(ammountRequest);
		int categoryID = Auxiliary.enterInt(categoryIDRequest);
		String date = Auxiliary.enterDate(dateRequest);
		String notes = Auxiliary.enterString(notesRequest);
		
		addIncome(ammount, destinyAccountID, categoryID, date, notes);
				
	}
	
	public static void addIncome(float ammount, int destinyAccountID, int categoryID, String date, String notes) {
		
		Database data = new Database();
		
		data.addIncome(ammount, destinyAccountID, categoryID, date, notes);
				
	}

}
