package expense_Manager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.Calendar;


public class Auxiliary {
	
	static Scanner input = new Scanner(System.in);
	final static String DATE_FORMAT = "yyyy-MM-dd";
	
	
	public static String enterDate(String request){
		
		int year = enterYear();
		String month = enterMonth();
		String day = enterDay();
		
		String date = year + "-" + month + "-" + day;
		
		System.out.println(date);
		
		if(isDateValid(date)){
			
			return date;
			
		} else {
			
			date = enterDate(request);
			return date;
		}
	}
	
	
	
	public static int enterYear(){
				
			int year = 0;
			int currentYear = Calendar.getInstance().get(Calendar.YEAR);
			
					
			String yearRequest = "Enter the year of the transaction";
			
			String instruction = Auxiliary.enterString(yearRequest);
			
			if (instruction.equals("")) {
				
				return currentYear;
				
			} else {
			
				try {
			
					year = Integer.parseInt(instruction);
						
				} catch (Exception e ) {
			
					System.out.println("You entered an invalid year. Going back to the Main Menu:");
			
					Auxiliary.switchIdle();
				}
				
				
				if (year > currentYear) {
					
					System.out.println("The transaction cannot be set in the future, try again.");
					enterYear();
				}
				
				else if (year < currentYear - 120) {
					
					System.out.println("I doubt you were alive in the year " + year);
					enterYear();
				}
				
				else {
					return year;
				}
			}
			
			return currentYear;
					
	}
	
	public static String enterMonth(){
		
		int month = 0;
		int currentMonth = (Calendar.getInstance().get(Calendar.MONTH) + 1);
		
		String monthRequest = "Enter the month of the transaction";
		
		String enteredText = Auxiliary.enterString(monthRequest);
		
		if (enteredText.equals("")) {
		
			return format00(currentMonth);
			
		} else {
		
			try {
		
				month = Integer.parseInt(enteredText);
					
			} catch (Exception e ) {
		
				System.out.println("You entered an invalid Month. Going back to the Main Menu:");
		
				Auxiliary.switchIdle();
			}
			
			
			if (month < 13 & month > 0) {
				
				return format00(month);
			}
			
			else {
				
				System.out.println("There are only 12 months in a year, Son.");
				return enterMonth();
			}
		}
				
	}
	
	public static String format00(int number){
		
		if(number > 9) {
			
			return "" + number;
		}
		
		else {
			
			return "0" + number;
		}
		
		
	}
	
	public static String enterDay(){
		
		int day = 0;
		int currentDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		
		String dayRequest = "Enter the number of the day of the transaction";
	
		String enteredText = Auxiliary.enterString(dayRequest);
		
		if (enteredText.equals("")) {
			
			return format00(currentDay);
		} else {
			
			try { 
			
				day = Integer.parseInt(enteredText);
				
			} catch (Exception e) {
				
				System.out.println("You entered an invalid day. Going back to the Main Menu:");
				
				Auxiliary.switchIdle();
			}
		}
		
		if (day < 32 & day > 0) {
			
			return format00(day);
			
		} else {
			
			System.out.println("Enter a valid day between 1 and 31");
			
			return enterDay();
			
		}
		
	}
	

	public static boolean isDateValid(String date) 
	{
	        try {
	            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
	            df.setLenient(false);
	            df.parse(date);
	            return true;
	        } catch (ParseException e) {
	            System.out.println("The date you entered is invalid.");
	        	return false;
	      
	        }
	}
	
	public static float enterFloat(String request){
		
		while(true) {
			System.out.println(request);
			
			try {
				float ammount = input.nextFloat();
				input.nextLine();
				return ammount;
			} catch (java.util.InputMismatchException e) {
				
				input.nextLine();
				
			}
		}
		
		
	}
	
	public static int enterInt(String request){
		
		while(true) {
			System.out.println(request);
			
			try {
				int number = input.nextInt();
				input.nextLine();
				return number;
			} catch (java.util.InputMismatchException e) {
				
				input.nextLine();
				//System.out.println(request);
			}
		}		
	}
	
	public static String enterString(String request) {
		
		while(true) {
			System.out.println(request);
			
			try {
				String someString = input.nextLine();
				return someString;
			} catch (java.util.InputMismatchException e) {
				
				input.nextLine();
				System.out.println(request);
			}
		}
	}
	
	public static void switchIdle(){
		
		if(ExpenseManager.isIdle()){
			
			ExpenseManager.setIdle(false);
			
		} else {
			
			ExpenseManager.setIdle(true);
			
		}
	}

	public static void showMainStats(){
		
		Auxiliary.switchIdle();
		
		Database data = new Database();
		
		data.showMainStats();
		
		ExpenseManager.showMainMenu();
				
	}
	
	public static void showAccounts(){
		
		Database data = new Database();
		
		data.showAccounts();
		
		ExpenseManager.selectAccount();
		
	}
	
	
	public static void showExpenses(){
		
		Database data = new Database();
		
		data.showExpenses();
		
		ExpenseManager.returnToMainMenu();
		
	}
	
	public static void showAccountExpenses(int accountID){
		
		Database data = new Database();
		
		data.showAccountExpenses(accountID);
		
		ExpenseManager.showAccountsMenu(accountID);
		
	}
	
	public static void showIncome(){
		
		Database data = new Database();
		
		data.showIncome();
		
		ExpenseManager.returnToMainMenu();
		
	}
	
	public static void addExpenseCategory(String category){
		
		Database data = new Database();
		
		data.addExpenseCategory(category);
		
		Auxiliary.switchIdle();
	}
	
	
}
