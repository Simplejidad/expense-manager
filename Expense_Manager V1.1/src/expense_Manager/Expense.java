package expense_Manager;

public class Expense {
	
	static String ammountRequest = "How much would did you spend? Write the number, then press Enter";
	static String originAccountIDRequest = "Where did the money for your expense come from?\nEnter the ID number of the account, then press Enter.";
	static String categoryIDRequest = "What kind of expense whas this? Enter de id of the category, then press Enter";
	static String notesRequest = "Enter any notes you may want to add to this transaction.";
	static String dateRequest = "When did you make this expense? Enter the date of the transaction in the format YYYY-MM-DD";
	
	public Expense(){
		
	}
	
	public static void addExpense(){
		
		Database data = new Database();
		
		float ammount = Auxiliary.enterFloat(ammountRequest);
		int originAccountID = Auxiliary.enterInt(originAccountIDRequest);
		int categoryID = Auxiliary.enterInt(categoryIDRequest);
		String date = Auxiliary.enterDate(dateRequest);
		String notes = Auxiliary.enterString(notesRequest);
		
		data.addExpense(ammount, originAccountID, categoryID, date, notes);
		
	}
	
	public static void addExpense(int originAccountID){
		
		Database data = new Database();
		
		float ammount = Auxiliary.enterFloat(ammountRequest);
		int categoryID = Auxiliary.enterInt(categoryIDRequest);
		String date = Auxiliary.enterDate(dateRequest);
		String notes = Auxiliary.enterString(notesRequest);
		
		data.addExpense(ammount, originAccountID, categoryID, date, notes);
		
		ExpenseManager.showAccountsMenu(originAccountID);
		
	}
	
	public static void addExpense(float ammount, int originAccountID, int categoryID, String date, String notes) {
		
		Database data = new Database();
		
		data.addExpense(ammount, originAccountID, categoryID, date, notes);

	}

}
